Sources for the web site liberabaci.gitlabpages.inria.fr

---

This web-site is organized around the following modus operandi.

 - Pages in `html` are stored in the directory `src`.  These pages are supposed to
  use a `css` style file also provided in this directory.

 - Documents in pdf are stored in the directory `documents`.

 - To modify the web-site, simply edit the `html` files available in `src` and
   add whatever `pdf` documents you want to make available in the `documents`
   directory.

 - Commit and push to `git@gitlab.inria.fr/liberabaci/liberabaci.gitlabpages.inria.fr` (`master` branch).  When the push is performed, the pages are
  updated, thanks to the directive given in the file `.gitlab-ci.yml`


In the current version, all files are in the same directory on the web-site, regardless of whether they come from the `documents` part or the `src` part of
the git repository.

In the long run, we can add an extra directory for Coq developments, but it is
also expected that each library will have its own web-site, which will be
referenced from here.
