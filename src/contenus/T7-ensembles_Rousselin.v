Set Keyed Unification.
Require Coq.Unicode.Utf8.
Require Import Coq.Logic.FunctionalExtensionality.
Require Import Coq.Logic.PropExtensionality.
Require Import Coq.Logic.Classical.

Require Import Coq.Arith.PeanoNat.

(** Ensembles *)

(** Attention, il peut y avoir plusieurs objectifs différents :
    - un fichier "isolé" pour travailler sur les ensembles en maths
    - une bibliothèque entière avec un langage "à la théorie des ensembles"
    Je (PR) soupçonne que la première approche ne convient qu'au premier *)

(** Les ensembles comme prédicats sur un type fixé. *)
(* On fixe un "gros" ensemble dont les autres seront des parties. *)
Definition Part E := E -> Prop.
Definition is_in {E : Type} (x : E) (A : Part E) := (A x).
Inductive empty_part {E : Type} : Part E :=.
Inductive full_part E : Part E := in_full (x : E) : is_in x (full_part E).

Notation "x ∈ A" := (is_in x A) (at level 50).
Notation "x ∉ A" := (~ is_in x A) (at level 50).
Notation "∅" := empty_part.

Lemma not_in_not {E : Type} (A : Part E) (x : E) : x ∉ A <-> ~(x ∈ A).
Proof. now split. Qed.

Lemma not_in_empty {E : Type} (x : E) : x ∉ ∅.
Proof. now intros []. Qed.

Coercion full_part : Sortclass >-> Part.

Lemma all_in_full {E : Type} (x : E) : x ∈ E.
Proof. easy. Qed.

Definition included {E : Type} (A B : Part E) :=
  forall (x : E), x ∈ A -> x ∈ B.
Notation "A ⊂ B" := (included A B) (at level 50).

Lemma empty_included_l {E : Type} (A : Part E) : ∅ ⊂ A.
Proof. now intros x. Qed.

Lemma included_refl {E : Type} (A : Part E) : A ⊂ A.
Proof. now intros x. Qed.

Lemma included_full_r {E : Type} (A : Part E) : A ⊂ E.
Proof. now intros x _. Qed.

(** Set extensionality, derived from fun_ext + prop_ext *)
Lemma set_extensionality {E : Type} (A B : Part E) :
  A = B <-> (A ⊂ B /\ B ⊂ A).
Proof.
  split.
  - now intros ->; split; apply included_refl.
  - intros [H1 H2]; apply functional_extensionality.
    intros x; apply propositional_extensionality.
    split; intros H; [apply H1 | apply H2]; exact H.
Qed.

(* Test: *)
Lemma Private_full_eq_full {E : Type} : (E : Part E) = E.
Proof. (* can it actually be proved by set_ext ? *)
  (* NOTE: ça ne fonctionne pas sans l'annotation de type ci-dessus *)
  apply set_extensionality; split; exact (included_full_r _).
Qed.

Lemma set_extensionality_pw {E : Type} (A B : Part E) :
  A = B <-> forall (x : E), (x ∈ A <-> x ∈ B).
Proof.
  split.
  - now intros ->.
  - now intros H; apply set_extensionality; split; intros x Hx; apply H.
Qed.

Lemma included_antisymm {E : Type} (A B : Part E) :
  A ⊂ B -> B ⊂ A -> A = B.
Proof. now intros H1 H2; apply set_extensionality; split. Qed.

Lemma included_trans {E : Type} (A B C : Part E) :
  A ⊂ B -> B ⊂ C -> A ⊂ C.
Proof. intros H1 H2 x Hx; apply H2; apply H1; exact Hx. Qed.

(** More tests about E seen as a Part of itself. *)

Section TestsEPartE.
  Context {E : Type}.
  Fail Check E ∈ Part E.
  Lemma Private_testEPE1 : (E : Part E) ∈ Part E.
  Proof. (* easy solves it. *)
    Set Printing All.
    unfold is_in.
    exact (all_in_full _).
  Qed.
  Fail Check Part E ∈ Part (Part E).
  Lemma Private_testEPE2 : (Part E : Part (Part E)) ∈ Part (Part E).
  Proof. (* easy solves it. *)
    Set Printing All.
    unfold is_in.
    exact (all_in_full _).
  Qed.
  (* Not very satisfactory but not *that* bad, if it is consistent and
     can be documented. *)
End TestsEPartE.

(** ** Union *)

Definition union {E : Type} (A B : Part E) : Part E :=
  fun x => x ∈ A \/ x ∈ B.
Notation "A ∪ B" := (union A B) (at level 40).

Lemma union_or {E : Type} (A B : Part E) (x : E) :
  x ∈ A ∪ B <-> x ∈ A \/ x ∈ B.
Proof.
  now split; intros [H1 | H2]; [left | right | left | right].
Qed.

Lemma union_right {E : Type} (A B : Part E) (x : E) :
  x ∈ B -> x ∈ A ∪ B.
Proof. now intros H; right. Qed.

Lemma union_left {E : Type} (A B : Part E) (x : E) :
  x ∈ A -> x ∈ A ∪ B.
Proof. now intros H; left. Qed.

Lemma incl_union_l {E : Type} (A B : Part E) : A ⊂ A ∪ B.
Proof. intros x Hx. apply union_left. exact Hx. Qed.

Lemma incl_union_r {E : Type} (A B : Part E) : B ⊂ A ∪ B.
Proof. intros x Hx. apply union_right. exact Hx. Qed.

Lemma union_full_l {E : Type} (B : Part E) : E ∪ B = E.
Proof.
  apply set_extensionality; split.
  - exact (included_full_r _).
  - exact (incl_union_l _ _).
Qed.

Lemma union_comm {E : Type} (A B : Part E) : A ∪ B = B ∪ A.
Proof.
  apply set_extensionality; split; intros x; intros H%union_or;
    apply union_or, or_comm; exact H.
Qed.

Lemma union_full_r {E : Type} (A : Part E) : A ∪ E = E.
Proof. rewrite union_comm; exact (union_full_l _). Qed.

Lemma union_empty_l {E : Type} (A : Part E) : ∅ ∪ A = A.
Proof.
  apply set_extensionality_pw; intros x; split.
  - intros [[] | H]%union_or; exact H.
  - intros H; apply union_or; right; exact H.
Qed.

Lemma union_empty_r {E : Type} (A : Part E) : A ∪ ∅ = A.
Proof. rewrite union_comm; exact (union_empty_l _). Qed.

Lemma union_assoc {E : Type} (A B C : Part E) :
  A ∪ (B ∪ C) = (A ∪ B) ∪ C.
Proof.
  apply set_extensionality_pw; intros x.
  (* très tentant, avec setoid_rewrite : rewrite !union_or, or_assoc. *)
  split.
  - intros [HA | [HB | HC]%union_or]%union_or; apply union_or.
    + now left; apply union_left.
    + now left; apply union_right.
    + now right.
  - intros [[HA | HB]%union_or | HC]%union_or; apply union_or.
    + now left.
    + now right; apply union_left.
    + now right; apply union_right.
Qed.

(** ** Intersection *)

Definition intersection {E : Type} (A B : Part E) : Part E :=
  fun (x : E) => x ∈ A /\ x ∈ B.

Notation "A ∩ B" := (intersection A B) (at level 40).

Lemma intersection_conj {E : Type} (A B : Part E) (x : E) :
  x ∈ A -> x ∈ B -> x ∈ A ∩ B.
Proof. now intros H1 H2; split. Qed.

Lemma intersection_and {E : Type} (A B : Part E) (x : E) :
  x ∈ A ∩ B <-> x ∈ A /\ x ∈ B.
Proof. now split; intros [H1 H2]; split. Qed.

Lemma incl_intersection_l {E : Type} (A B : Part E) :
  A ∩ B ⊂ A.
Proof. now intros x [H _]. Qed.

Lemma incl_intersection_r {E : Type} (A B : Part E) :
  A ∩ B ⊂ B.
Proof. now intros x [_ H]. Qed.

Lemma intersection_comm {E : Type} (A B : Part E) :
  A ∩ B = B ∩ A.
Proof.
  now apply set_extensionality_pw; intros x; split; intros [H1 H2]; split.
Qed.

Lemma intersection_full_l {E : Type} (B : Part E) :
  E ∩ B = B.
Proof.
  apply set_extensionality; split; [exact (incl_intersection_r _ _) |].
  now intros x Hx; split.
Qed.

Lemma intersection_full_r {E : Type} (A : Part E) :
  A ∩ E = A.
Proof. now rewrite intersection_comm; exact (intersection_full_l _). Qed.

Lemma intersection_empty_r {E : Type} (A : Part E) :
  A ∩ ∅ = ∅. 
Proof.
  now apply set_extensionality; split; intros x; [intros [_ []] | intros []].
Qed.

Lemma intersection_empty_l {E : Type} (B : Part E) :
  ∅ ∩ B = ∅.
Proof. now rewrite intersection_comm; exact (intersection_empty_r _). Qed.

(** Complémentaire *)

Definition complementary {E : Type} (A : Part E) : Part E :=
  fun x => x ∉ A.

(* for some reason I cannot use Aᶜ *)
Notation "A 'ᶜ'" := (complementary A) (at level 15).

Lemma complementary_not_iff {E : Type} (A : Part E) (x : E) :
  x ∈ A ᶜ <-> x ∉ A.
Proof. now split. Qed.

Lemma complementary_not {E : Type} (A : Part E) (x : E) :
  x ∈ A ᶜ -> x ∉ A.
Proof. easy. Qed.

Lemma not_complementary {E : Type} (A : Part E) (x : E) :
  x ∉ A -> x ∈ A ᶜ.
Proof. easy. Qed.

Lemma PNNP (P : Prop) : P -> ~(~ P).
Proof. intros H H'. exact (H' H). Qed.

Lemma complementary_involutive {E : Type} (A : Part E) :
  (A ᶜ) ᶜ = A.
Proof.
  apply set_extensionality; split.
  - intros x H.
    (* Sans magie c'est dur:
    apply complementary_not_iff in H. (* ne change rien *)
    Fail apply ->complementary_not_iff in H.
    apply ->(@complementary_not_iff E) in H.
    Fail apply ->not_in_not in H.
    apply ->(@not_in_not E) in H. (* ne change rien *)
    unfold "∉" in H. (* unfold = échec *)
    (* Comment remplacer x ∈ A ᶜ par x ∉ A sans setoid_rewrite ? *)
    rewrite complementary_not_iff in H.
    Fail rewrite ->(@not_in_not E) in H. (* failed to progress *)
    *)
    apply NNPP in H. (* magie *)
    exact H.
  - intros x H. 
    apply PNNP.
    exact H.
Qed.

(** Différence *)

Definition subtraction {E : Type} (A B : Part E) :=
  fun x => (x ∈ A /\ x ∉ B).

Notation "A ⧵ B" := (subtraction A B) (at level 15).

Lemma subtraction_def {E : Type} (A B : Part E) (x : E) :
  x ∈ A ⧵ B <-> x ∈ A /\ x ∉ B.
Proof. easy. Qed.

Lemma subtraction_complementary {E : Type} (A B : Part E) :
  A ⧵ B = A ∩ B ᶜ.
Proof.
  apply set_extensionality_pw; intros x.
  (* allez, setoid_rewrite... *)
  rewrite subtraction_def, intersection_and, complementary_not_iff.
  (* tant qu'à faire... *)
  reflexivity.
Qed.

(** Quelques exercices *)

Module Question1_a_c.
  Lemma Private_question1_a :
    ~(forall (E : Type) (A B C D : Part E), A ⊂ B \/ C ⊂ D -> A ∩ C ⊂ B ∩ D).
  Proof.
    intros H.
    specialize (H nat nat nat nat ∅).
    rewrite intersection_empty_r in H. (* ok : égalité *)
    rewrite intersection_full_r in H. (* ok : égalité *)
    lapply H.
    + intros H'.
      specialize (H' 0).
      apply (not_in_empty 0), H'. easy.
    + now left.
  Qed.

  Lemma Private_question1_c :
    forall (E : Type) (A B : Part E), B ⊂ A -> A ⊂ A ∪ B.
  Proof. intros E A B _ x Hx; left; exact Hx. Qed.
End Question1_a_c.

(** produit cartésien *)

About "_ * _"%type.
Print prod.

Definition rectangle {E F : Type} (A : Part E) (B : Part F) : Part (E * F) :=
  fun (p : E * F) =>
    match p with
      pair x y => (x ∈ A) /\ (y ∈ B)
    end.

Notation "A × B" := (rectangle A B) (at level 20).

Lemma in_rectangle {E F : Type} (A : Part E) (B : Part F) (x : E) (y : F) :
  pair x y ∈ A × B <-> x ∈ A /\ y ∈ B.
Proof. now split. Qed.

Module Question1_b.
  Lemma Private_question1_b {E F : Type} (A : Part E) (B : Part F) :
    A ᶜ × B ᶜ = ((A × F) ∪ (E × B)) ᶜ.
  Proof.
    apply set_extensionality_pw; intros [x y]; split.
    - intros [H1%complementary_not H2%complementary_not]%in_rectangle.
      apply not_complementary.
      now intros [[H1' H1'']%in_rectangle | [H2' H2'']%in_rectangle].
    - intros H%complementary_not.
      apply in_rectangle; split; intros H'; apply H.
      + left; apply in_rectangle; split; [exact H' | exact (all_in_full y)].
      + right; apply in_rectangle; split; [exact (all_in_full x) | exact H'].
  Qed.
End Question1_b.

(** Une relation d'équivalence sur les parties *)

Definition eq_or_compl {E : Type} (A B : Part E) :=
  A = B \/ A = B ᶜ.

Notation "A ⟂ B" := (eq_or_compl A B) (at level 80).

Definition reflexive {T : Type} (R : T -> T -> Prop) := forall (x : T), R x x.
Definition symmetric {T : Type} (R : T -> T -> Prop) :=
  forall (x y : T), R x y -> R y x.
Definition transitive {T : Type} (R : T -> T -> Prop) :=
  forall (x y z : T), R x y -> R y z -> R x z.

Definition equivalence {T : Type} (R : T -> T -> Prop) :=
  reflexive R /\ symmetric R /\ transitive R.

Lemma question1_e {E : Type} : equivalence (@eq_or_compl E).
Proof.
  split; [| split].
  - intros x; left; reflexivity.
  - intros x y [H | H].
    + left; rewrite H; reflexivity.
    + right; rewrite H; rewrite complementary_involutive; reflexivity.
  - intros x y z H1 H2.
    destruct H1 as [H1 | H1].
    + rewrite H1. exact H2.
    + destruct H2 as [H2 | H2].
      * right. rewrite <-H2. exact H1.
      * left. rewrite H1, H2, complementary_involutive. reflexivity.
Qed.
