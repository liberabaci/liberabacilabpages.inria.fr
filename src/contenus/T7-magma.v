From HB Require Import structures.
From Coq Require Import ssreflect ssrbool ssrfun.
From elpi.apps Require Import tc coercion.
Require Import sets.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Local Open Scope set_scope.

Declare Scope monoid_scope.
Delimit Scope monoid_scope with M.
Local Open Scope monoid_scope.

Reserved Notation "[ 'mag' M >-> N ]"
  (at level 0, M, N at level 99, format "[ 'mag'  M  >->  N ]").



(********************************** Magmas ***********************************)

HB.mixin Record isMagma (T : Type) := {
  op : T -> T -> T
}.

#[short(type="magmaType")]
HB.structure Definition Magma := {T of isMagma T}.

Notation "x * y" := (@op _ x y) : monoid_scope.
Notation "x *_ T y" := (@op T%type (x : T) (y : T))
  (T at level 0, at level 60, format "x  *_ T  y", only parsing) : monoid_scope.

Definition set_magma (M : magmaType) := isMagma.Build (set M)
  (fun X Y => [set x *_M y | x in X & y in Y]).

HB.instance Definition _ M := @set_magma M.

Check forall (M : magmaType) (X Y : set M), X * Y = Y * X.

Definition prod_magma (M N : magmaType) := isMagma.Build (M * N)%type
  (fun x y => (x.1 * y.1, x.2 * y.2)).

HB.instance Definition _ M N := @prod_magma M N.

Check forall (M N : magmaType) (x y : (M * N)%type), x * y = y * x.

Definition pow_magma (I : Type) (M : I -> magmaType) :=
  isMagma.Build (forall i, M i) (fun f g i => f i * g i).

HB.instance Definition _ I M := @pow_magma I M.

Check forall (I : Type) (M : I -> magmaType) (f g : forall i, M i),
  f * g = g * f.



(****************************** Magma morphisms ******************************)

Notation "[ 'mag' M >-> N ]" :=
  ([set f : M -> N |
      forall x y : M, f (x * y) = f x * f y]) : set_scope.

Lemma magma_morphM (M N : magmaType) (f : [mag M >-> N]) :
  forall x y, f (x * y) = f x * f y.
Proof. by move: (memP f); rewrite setE. Qed.

Lemma id_magma_morph (M : magmaType) : idfun \in [mag M >-> M].
Proof. by rewrite setE. Qed.

HB.instance Definition _ M := @id_magma_morph M.

Check fun (M : magmaType) => @idfun M : [mag M >-> M].

Lemma comp_magma_morph (M N T : magmaType)
    (f : [mag M >-> N]) (g : [mag N >-> T]) :
  (g \o f) \in [mag M >-> T].
Proof. by rewrite setE => x y; rewrite /comp 2!magma_morphM. Qed.

HB.instance Definition _ M N T f g := @comp_magma_morph M N T f g.

Check fun (M N T : magmaType) (f : [mag M >-> N]) (g : [mag N >-> T]) =>
  g \o f : [mag M >-> T].



(********************************* Submagmas *********************************)

Definition magmaSet (M : magmaType) :=
  [set X : set M | forall (x y : X), x *_M y \in X].

Section magmaSetTheory.

Lemma memM (M : magmaType) (X : magmaSet M) (x y : X) : x *_M y \in X.
Proof. by move: (memP X); rewrite setE. Qed.

HB.instance Definition _ M X x y := @memM M X x y.

Definition magmaSet_isMagma (M : magmaType) (X : magmaSet M) :=
  isMagma.Build X (fun x y => x *_M y).

HB.instance Definition _ M X := @magmaSet_isMagma M X.

Check fun (M : magmaType) (X : magmaSet M) (x y : X) => x * y = x.

End magmaSetTheory.

Lemma fst_magmaSet (M N : magmaType) (X : magmaSet (M * N)%type) :
  [set fst x | x in X] \in magmaSet M.
Proof.
rewrite setE => [[x]] /[swap] [[y]]/=.
by rewrite 3!setE => [[x']] <- [y'] <-; exists (y' * x').
Qed.

HB.instance Definition _ M N X := @fst_magmaSet M N X.

Lemma snd_magmaSet (M N : magmaType) (X : magmaSet (M * N)%type) :
  [set snd x | x in X] \in magmaSet N.
Proof.
rewrite setE => [[x]] /[swap] [[y]]/=.
by rewrite 3!setE => [[x']] <- [y'] <-; exists (y' * x').
Qed.

HB.instance Definition _ M N X := @snd_magmaSet M N X.
