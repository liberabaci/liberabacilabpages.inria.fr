import { ExecCore, ExecCoreOptions } from 'wasi-kernel';
import { Environ } from 'wasi-kernel/lib/kernel/exec';
interface OCamlCAPI {
    malloc(sz: i32): i32;
    free(p: i32): void;
    caml_alloc_string(len: i32): i32;
    caml_named_value(name: i32): i32;
    caml_callback(closure: i32, arg: i32): i32;
}
declare namespace OCamlCAPI {
    function Val_int(v: number): i32;
    function Val_bool(b: boolean): i32;
    function Int_val(v: i32): number;
    function Bool_val(v: i32): boolean;
    const Val_unit: number;
    const Val_false: number;
    const Val_true: number;
}
declare type i32 = number;
declare class OCamlExecutable extends ExecCore {
    opts: OCamlExecutableOptions;
    api: OCamlCAPI;
    callbacks: {
        [name: string]: (arg: i32) => i32;
    };
    constructor(opts: OCamlExecutableOptions);
    initialEnv(): Environ;
    run(bytecodeFile: string, args: string[], callbacks?: string[]): Promise<void>;
    preloads(): {
        name: string;
        uri: string;
        reloc?: any;
    }[];
    to_caml_string(s: string): number;
    _getCallbacks(names: string[]): {
        [name: string]: (arg: i32) => i32;
    };
}
declare type OCamlExecutableOptions = ExecCoreOptions & {
    binDir?: string;
};
export { OCamlExecutable, OCamlCAPI };
//# sourceMappingURL=ocaml_exec.d.ts.map