/**
 * Allows to signal the worker that it is requested to interrupt its current
 * computation.
 * The page signals the worker by incrementing a 32-bit shared integer.
 * The worker periodically checks the integer, and would eventually break
 * its execution if the integer has been modified since the last time it was
 * read.
 */
declare class WorkerInterrupts {
    vec: Uint32Array;
    checkpoint: number;
    setup(vec: Uint32Array): void;
    pending(): boolean;
}
export { WorkerInterrupts };
//# sourceMappingURL=interrupt.d.ts.map