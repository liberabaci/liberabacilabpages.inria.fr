/// <reference types="node" />
import { EventEmitter } from 'events';
import { OCamlExecutable } from './ocaml_exec';
import { WorkerInterrupts } from './interrupt';
declare class IcoqPod extends EventEmitter {
    core: OCamlExecutable;
    intr: WorkerInterrupts;
    binDir: string;
    nmDir: string;
    io: IO;
    constructor(binDir?: string, nmDir?: string, fetchMode?: FetchMode);
    findlibStartup(): Promise<void>;
    get fs(): import("memfs").IFs;
    boot(): Promise<void>;
    upload(fromUri: string, toPath: string): Promise<void>;
    loadPackage(uri: string, refresh?: boolean): Promise<void>;
    loadPackages(uris: string | string[], refresh?: boolean): Promise<void>;
    loadSources(uri: string, dirpath: string): Promise<void>;
    unzip(uri: string, dir: string): Promise<void>;
    _pkgUri(uri: string): string;
    _progress(uri: string, download: DownloadProgress): void;
    putFile(filename: string, content: Uint8Array | string): void;
    getFile(filename: string): void;
    command(cmd: any[]): void;
    answer(msgs: any[][]): void;
    _answer(ptr: number): void;
    _interrupt_pending(): number;
    /**
     * (internal) Initializes the dllcoqrun_stub shared library.
     */
    _preloadStub(): void;
}
declare class IO {
    fetchMode: FetchMode;
    pending: Set<ReadableStreamDefaultReader<Uint8Array>>;
    constructor(fetchMode: FetchMode);
    unzip(uri: string, put: (filename: string, content: Uint8Array) => void, progress?: (p: any) => void): Promise<void>;
    _fetch(uri: string, progress?: (p: any) => void): Promise<Uint8Array>;
    _toU8A(blob: Promise<Blob>): Promise<Uint8Array>;
    _fetchSimple(uri: string): Promise<any>;
    _fetchWithProgress(uri: string, progress: (p: any) => void): Promise<Blob>;
}
declare type FetchMode = 'browser' | 'fs';
declare type DownloadProgress = {
    total: number;
    downloaded: number;
};
export { IcoqPod, DownloadProgress };
//# sourceMappingURL=core.d.ts.map