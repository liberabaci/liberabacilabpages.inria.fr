export declare class Future<A> {
    promise: Promise<A>;
    _resolve: (value: A | PromiseLike<A>) => void;
    _reject: (reason?: any) => void;
    _done: boolean;
    _success: boolean;
    constructor();
    resolve(val: any): void;
    reject(err?: any): void;
    then(cont: any): Promise<A>;
    isDone(): boolean;
    isSuccessful(): boolean;
    isFailed(): boolean;
}
declare type RouteMessage = {
    sid: number;
    lvl: string;
    loc: any;
    msg: string;
};
export declare class PromiseFeedbackRoute<A> extends Future<A> {
    atexit: (_: void) => void;
    messages: RouteMessage[];
    _got_processed: boolean;
    constructor();
    feedMessage(sid: any, lvl: any, loc: any, msg: any): void;
    feedComplete(sid: any): void;
    feedIncomplete(sid: any): void;
    feedProcessed(sid: any): void;
    feedSearchResults(bunch: any): void;
}
export {};
//# sourceMappingURL=future.d.ts.map