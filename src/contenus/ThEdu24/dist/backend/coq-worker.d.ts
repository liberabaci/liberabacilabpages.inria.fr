export declare type backend = 'js' | 'wa';
declare type Block_type = [
    'Pp_hbox'
] | ['Pp_vbox', number] | ['Pp_hvbox', number] | ['Pp_hovbox', number];
export declare type Pp = [
    'Pp_empty'
] | ['Pp_string', string] | ['Pp_glue', Pp[]] | ['Pp_box', Block_type, Pp] | ['Pp_tag', string, Pp] | ['Pp_print_break', number, number] | ['Pp_force_newline'] | ['Pp_comment', string[]];
/**
 * Main Coq Worker Class
 *
 */
declare type CoqEventObserver = Object;
export declare class CoqWorkerConfig {
    path: URL;
    preload: URL[];
    backend: backend;
    debug: boolean;
    warn: boolean;
    constructor(basePath: string | URL, backend: backend);
    /**
     * Default location for worker script -- computed relative to the URL
     * from which this script is loaded.
     */
    static determineWorkerPath(basePath: string | URL, backend: backend): URL;
    getPreloads(basePath: string | URL, backend: backend): URL[];
}
export declare class CoqWorker {
    config: CoqWorkerConfig;
    protected worker: Worker;
    intvec: Int32Array;
    private load_progress;
    private _boot;
    protected when_created: Promise<void>;
    protected _handler: (msg: any) => void;
    observers: CoqEventObserver[];
    private routes;
    private sids;
    private _gen_rid;
    constructor(base_path: (string | URL), scriptPath: URL, worker: any, backend: backend);
    /**
     * Adjusts a given URI so that it can be requested by the worker.
     * (the worker may have a different base path than the page.)
     */
    resolveUri(uri: string | URL): string;
    /**
     * Creates the worker object
     *
     * @param {string} script_path
     * @return {Promise<Worker>}
     * @async
     * @memberof CoqWorker
     */
    createWorker(script_path: any): Promise<void>;
    /**
     * @param {string} url
     */
    newWorkerWithProgress(url: URL, preload: URL[]): Promise<Worker>;
    /**
     * @param {Worker} worker
     */
    attachWorker(worker: any): void;
    /**
     * Sends a Command to Coq worker
     *
     * @param {object[]} msg
     * @memberof CoqWorker
     */
    sendCommand(msg: any): void;
    /**
     * @param {any[]} msg
     */
    sendDirective(msg: any): void;
    /**
     * Send Init Command to Coq
     *
     * @param {object} coq_opts
     * @param {object} doc_opts
     * @memberof CoqWorker
     */
    init(coq_opts: any, doc_opts: any): void;
    getInfo(): void;
    /**
     * @param {any} ontop_sid
     * @param {string | number} new_sid
     * @param {any} stm_text
     * @param {boolean} resolve
     */
    add(ontop_sid: any, new_sid: any, stm_text: any, resolve?: boolean): void;
    /**
     * @param {any} ontop_sid
     * @param {any} new_sid
     * @param {any} stm_text
     */
    resolve(ontop_sid: any, new_sid: any, stm_text: any): void;
    /**
     * @param {any} sid
     */
    exec(sid: any): void;
    /**
     * @param {number} sid
     */
    cancel(sid: any): void;
    /**
     * @param {any} sid
     */
    goals(sid: any): void;
    /**
     * @param {number} sid
     * @param {any} rid
     * @param {any[]} query
     */
    query(sid: any, rid: any, query: any): any;
    inspect(sid: any, rid: any, search_query: any): any;
    /**
     * @param {string | string[]} option_name
     */
    getOpt(option_name: any): void;
    /**
     * @param {{base_path: string, pkg: string} | string} url
     */
    loadPkg(url: any): void;
    /**
     * @param {any} base_path
     * @param {any} pkgs
     */
    infoPkg(base_path: any, pkgs: any): void;
    /**
     * @param {any} load_path
     */
    refreshLoadPath(load_path: any): void;
    /**
     * @param {string} filename
     * @param {Buffer} content
     */
    put(filename: any, content: any, transferOwnership?: boolean): void;
    /**
     * @param {Buffer} buffer
     */
    arrayBufferOfBuffer(buffer: any): any;
    /**
     * @param {any} filename
     */
    register(filename: any): void;
    interruptSetup(): void;
    interrupt(): void;
    restart(): Promise<void>;
    end(): void;
    /**
     * @param {string | number} sid
     */
    execPromise(sid: any): Promise<void>;
    /**
     * @param {any} sid
     * @param {any} rid
     * @param {any} query
     */
    queryPromise(sid: any, rid: any, query: any): Promise<unknown>;
    /**
     * @param {any} sid
     * @param {any} rid
     * @param {any} search_query
     */
    inspectPromise(sid: any, rid: any, search_query?: any): Promise<unknown>;
    /**
     * @param {string | number} rid
     */
    _wrapWithPromise(rid: any): Promise<unknown>;
    join(child: any): void;
    /**
     * @param {{ data: any; }} evt
     */
    coq_handler(evt: any): void;
    coqBoot(): void;
    /**
     * @param {{ contents: string | any[]; route: number; span_id: any; }} fb_msg
     * @param {any} in_mode
     */
    coqFeedback(fb_msg: any, in_mode: any): void;
    /**
     * @param {string | number} rid
     * @param {any} bunch
     */
    coqSearchResults(rid: any, bunch: any): void;
    /**
     * @param {string | number} sid
     */
    feedProcessed(sid: any): void;
}
/**
 * This class is meant to support running the worker as a native subprocess,
 * available in waCoq through the `subproc` module.
 */
export declare class CoqSubprocessAdapter extends CoqWorker {
    constructor(base_path: any, backend: any);
    coq_handler(evt: any): void;
}
export {};
//# sourceMappingURL=coq-worker.d.ts.map