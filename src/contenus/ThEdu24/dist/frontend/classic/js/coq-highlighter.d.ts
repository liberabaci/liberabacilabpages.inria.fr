import './mode/coq-mode.js';
import 'codemirror/lib/codemirror.css';
import 'codemirror/addon/runmode/runmode.js';
export declare class CoqHighlighter {
    highlight(): void;
}
//# sourceMappingURL=coq-highlighter.d.ts.map