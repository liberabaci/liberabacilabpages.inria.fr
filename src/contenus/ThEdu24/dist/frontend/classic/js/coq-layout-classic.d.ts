/// <reference types="jquery" />
import { SettingsPanel } from './settings.js';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min.js';
import "../css/landing-page.css";
import "../css/kbd.css";
import '../css/coq-log.css';
import '../css/coq-base.css';
import '../css/coq-light.css';
import '../css/coq-dark.css';
import '../css/settings.css';
/***********************************************************************/
/***********************************************************************/
/**
 * Classical layout: a right panel containing a toolbar with buttons at the
 * top, and a main area divided vertically into three collapsible panes.
 * Also shows a power button that hides or shows the panel.
 *
 * @class CoqLayoutClassic
 */
export declare class CoqLayoutClassic {
    options: any;
    ide: HTMLElement;
    panel: HTMLDivElement;
    proof: HTMLDivElement;
    query: HTMLDivElement;
    packages: HTMLDivElement;
    buttons: HTMLSpanElement;
    menubtn: SVGElement;
    settings: SettingsPanel;
    onToggle: (evt: any) => void;
    onAction: (evt: any) => void;
    log_levels: string[];
    log_level: number;
    outline: HTMLDivElement;
    scrollTimeout?: any;
    html(params: any): string;
    /**
     * Initializes the UI layout.
     * @param {object} options jsCoq options; used keys are:
     *   - wrapper_id: element id of the IDE container
     *   - base_path: URL for the root directory of jsCoq
     *   - theme: jsCoq theme to use for the panel ('light' or 'dark')
     * @param {object} params HTML template parameters; used keys are:
     *   - kb: key-binding tooltips for actions {up, down, cursor}
     */
    constructor(options: any, params: any);
    /**
     * Configure or re-configure UI based on CoqManager options.
     * @param {object} options
     */
    configure(options: any): void;
    show(): void;
    hide(): void;
    isVisible(): boolean;
    toggle(): void;
    showHelp(): void;
    hideHelp(): void;
    toggleHelp(): void;
    splash(version_info: any, msg: any, mode?: string): void;
    splashLinks(): void;
    createOutline(): HTMLDivElement;
    /**
     * Shows a notice in the main goal pane (reserved for important messages,
     * such as during startup).
     * @param {string} msg message text
     */
    systemNotification(msg: any): void;
    _setButtons(enabled: any): void;
    toolbarOn(): void;
    toolbarOff(): void;
    update_goals(content: any): void;
    log(text: any, level: any, attrs?: any): JQuery<HTMLElement>;
    logSep(sid: any, item: any, prev: any): void;
    /**
     * Readjusts separators for the entire log when the level changes.
     * (called from filterLog)
     */
    logSepReadjust(): void;
    filterLog(level_select: any): void;
    isLogVisible(level: any): boolean;
    panelClickHandler(evt: any): void;
    /**
     * Set up hooks for when user changes settings.
     */
    _setupSettings(): void;
    /**
     * Auxiliary function to improve UX by preloading images.
     */
    _preloadImages(): void;
}
//# sourceMappingURL=coq-layout-classic.d.ts.map