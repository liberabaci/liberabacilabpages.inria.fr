/// <reference types="jquery" />
/// <reference types="jquery" />
import { CoqWorker, backend } from '../../../backend';
interface BundleInfo {
    row: string;
}
export declare class PackageManager {
    /**
     * Creates the packages UI and loading manager.
     *
     * @param {Element} panel_dom <div> element to hold the package entries
     * @param {object} packages an object containing package URLs and lists of
     *   names in the format
     *   `{'base_uri1', ['pkg_name1', 'pkg_name2', ...], 'base_uri2': ...}`.
     * @param {object} pkg_path_aliases mnemonic for specific base URIs
     * @param {CoqWorker} coq reference to the `CoqWorker` instance to send
     *   load requests to
     */
    backend: backend;
    panel: any;
    bundles: Map<string, BundleInfo>;
    loaded_pkgs: string[];
    coq: CoqWorker;
    packages: CoqPkgInfo[];
    packages_by_name: any;
    packages_by_uri: any;
    index?: PackageIndex;
    constructor(panel_dom: any, packages: any, pkg_path_aliases: any, coq: any, backend?: any);
    /**
     * Creates CoqPkgInfo objects according to the paths in names in the given
     * `packages` object.
     * @param {object} packages (see constructor)
     * @param {object} aliases (ditto)
     */
    initializePackageList(packages: string[], aliases?: {}): void;
    /**
     * Returns the default package path
     *
     * @param {string} base_path root path of jscoq package
     * @return {string}
     * @memberof PackageManager
     */
    static defaultPkgPath(base_path: any): string;
    populate(): Promise<void[]>;
    /**
     * Adds a package
     *
     * @param {*} pkg
     * @memberof PackageManager
     */
    addPackage(pkg: any): void;
    getPackage(pkg_name: any): any;
    hasPackageInfo(pkg_name: any): any;
    addRow(bname: any, desc: any, parent: any): {
        row: JQuery<HTMLElement>;
    };
    addBundleInfo(bname: string, pkg_info: CoqPkgInfo, parent?: {
        row: JQuery<HTMLElement>;
    }): void;
    addBundleZip(bname: string, resource: any, pkg_info?: CoqPkgInfo): Promise<CoqPkgInfo>;
    waitFor(init_pkgs: any): Promise<unknown>;
    getUrl(pkg_name: any, resource: any): any;
    getLoadPath(): any;
    showPackage(bname: any): void;
    _scrollTo(el: any): void;
    /**
     * Updates the download progress bar on the UI.
     * @param {string} bname package bundle name
     * @param {object} info? {loaded: <number>, total: <number>}
     */
    showPackageProgress(bname: any, info: any): void;
    /**
     * Marks the package download as complete, removing the progress bar.
     * @param {string} bname package bundle name
     */
    showPackageCompleted(bname: any): void;
    showLoadedChunks(pkg: any): void;
    /**
     * Adds a package from a dropped file and immediately downloads it.
     * @param {File} file a dropped File or a Blob that contains an archive
     */
    dropPackage(file: any): void;
    _packageByURL(url: any): string;
    _absoluteURL(url: any): string;
    coqLibProgress(evt: any): void;
    coqLibLoaded(pkg: any): void;
    coqLibError(pkg: any): void;
    /**
     * Loads a package from the preconfigured path.
     * @param {string} pkg_name name of package (e.g., 'init', 'mathcomp')
     * @param {boolean} show if `true`, the package is exposed in the list
     */
    loadPkg(pkg_name: any, show?: boolean): any;
    loadDeps(deps: any, show?: boolean): Promise<any[]>;
    loadArchive(pkg: any): any;
    /**
     * Make all loaded packages unloaded.
     * This is called after the worker is restarted.
     * Does not drop downloaded/cached archives.
     */
    reset(): void;
    collapse(): void;
    expand(): void;
    _expandCollapseRow(row: any): void;
    /**
     * (auxiliary method) traverses a graph spanned by a list of roots
     * and an adjacency functor. Implements DFS.
     * @param {array} roots starting points
     * @param {function} adjacent_out u => array of successors
     */
    _scan(roots: any, adjacent_out: any): Set<unknown>;
    dispatchEvent(evt: any): void;
    addEventListener(type: any, cb: any): void;
    removeEventListener(type: any, cb: any): void;
}
/**
 * Holds list of modules in packages and resolves dependencies.
 */
declare class PackageIndex {
    backend: backend;
    moduleIndex: Map<any, any>;
    intrinsicPrefix: string;
    constructor(backend: any);
    add(pkgInfo: any): void;
    findModules(prefix: any, suffix: any, exact?: boolean): Generator<any, void, unknown>;
    findPackageDeps(prefix: any, suffix: any, exact?: boolean): Set<unknown>;
    alldeps(mods: any): any;
}
declare class CoqPkgInfo {
    name: string;
    base_uri: string;
    info?: any;
    archive?: any;
    chunks?: any;
    parent?: any;
    promise?: any;
    constructor(name: any, base_uri: any);
    getUrl(resource: any): URL;
    getDownloadURL(): any;
    fetchInfo(resource?: string): Promise<any>;
    setArchive(resource?: string): void;
}
export {};
//# sourceMappingURL=coq-packages.d.ts.map