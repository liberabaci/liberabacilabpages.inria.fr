import CodeMirror from "codemirror";
import 'codemirror/addon/hint/show-hint.js';
import 'codemirror/addon/edit/matchbrackets.js';
import 'codemirror/keymap/emacs.js';
import 'codemirror/addon/selection/mark-selection.js';
import 'codemirror/addon/edit/matchbrackets.js';
import 'codemirror/addon/dialog/dialog.js';
import 'codemirror/addon/search/search.js';
import 'codemirror/addon/search/searchcursor.js';
import 'codemirror/addon/search/jump-to-line.js';
import 'codemirror/lib/codemirror.css';
import 'codemirror/theme/blackboard.css';
import 'codemirror/theme/darcula.css';
import 'codemirror/addon/hint/show-hint.css';
import 'codemirror/addon/dialog/dialog.css';
import '../external/CodeMirror-TeX-input/addon/hint/tex-input-hint.js';
import './mode/coq-mode.js';
import { CompanyCoq } from './addon/company-coq.js';
/**
 * A Coq sentence, typically ended in dot "."
 *
 * @class CmSentence
 */
declare class CmSentence {
    start: CodeMirror.Position;
    end: CodeMirror.Position;
    text: string;
    mark?: number;
    flags: {
        is_comment: boolean;
        is_hidden: () => boolean;
    };
    feedback: number[];
    action: number;
    coq_sid: string;
    /**
     * Creates an instance of CmSentence.
     * @param {CodeMirror.Position} start
     * @param {CodeMirror.Position} end
     * @param {string} text
     * @param {object} flags
     * @memberof CmSentence
     */
    constructor(start: any, end: any, text: any, flags: any);
}
declare module "codemirror" {
    var keyMap: any;
    interface TextMarker<T = CodeMirror.MarkerRange | CodeMirror.Position> {
        stm: CmSentence;
        diag: boolean;
        widgetNode: HTMLElement;
    }
    interface Editor {
        findMarks(from: any, to: any, filter: any): any;
        on(method: 'hintHover', fn: any): any;
        on(method: 'hintZoom', fn: any): any;
        on(method: 'hintEnter', fn: any): any;
        on(method: 'hintOut', fn: any): any;
    }
    interface OpenDialogOptions {
        value: string;
    }
}
/**
 * A CodeMirror-based Provider of coq statements.
 *
 * @class CmCoqProvider
 *
 */
export declare class CmCoqProvider {
    idx: number;
    dirty: boolean;
    autosave: any;
    autosave_interval: number;
    editor: CodeMirror.Editor;
    onChange: (cm: any, change: any) => void;
    onInvalidate: (evt: any) => void;
    onMouseEnter: (stm: any, evt: any) => void;
    onMouseLeave: (stm: any, evt: any) => void;
    onTipHover: (entrires: any, zoom: any) => void;
    onTipOut: (evt: any) => void;
    onResize: (evt: any) => void;
    onAction: (evt: any) => void;
    _keyHandler: (x: any) => void;
    _key_bound: boolean;
    hover: any[];
    company_coq?: CompanyCoq;
    lineCount: number;
    /**
     * Creates an instance of CmCoqProvider.
     * @param {*} element
     * @param {*} options
     * @param {*} replace
     * @param {number} idx
     * @memberof CmCoqProvider
     */
    constructor(element: any, options: any, replace: any, idx: any);
    /**
     * @param {HTMLElement} element
     * @param {CodeMirror.EditorConfiguration | undefined} opts
     * @param {any} replace
     */
    createEditor(element: any, opts: any, replace: any): CodeMirror.Editor;
    /**
     * @param {{ theme: any; }} options
     */
    configure(options: any): void;
    getText(): string;
    trackLineCount(): void;
    focus(): void;
    isHidden(): () => boolean;
    /**
     * @param {{ end: any; }} prev
     * @param {CodeMirror.Position} until
     */
    getNext(prev: any, until: any): CmSentence;
    getAtPoint(): CmSentence;
    /**
     * @param {{ mark: { find: () => any; clear: () => void; } | null; start: any; end: any; }} stm
     * @param {string} mark_type
     * @param {undefined} [loc_focus]
     */
    mark(stm: any, mark_type: any, loc_focus?: any): void;
    /**
     * @param {{ mark: { className: string; }; coq_sid: any; }} stm
     * @param {boolean} flag
     */
    highlight(stm: any, flag: any): void;
    /**
     * @param {any} stm
     * @param {any} loc
     * @param {string} className
     */
    squiggle(stm: any, loc: any, className: any): CodeMirror.TextMarker<CodeMirror.MarkerRange>;
    /**
     * Removes all sentence marks
     */
    retract(): void;
    /**
     * @param {{ coq_sid?: any; mark?: any; start?: any; end?: any; }} stm
     * @param {string} className
     */
    markWithClass(stm: any, className: any): void;
    /**
     * @param {{ mark: { on: (arg0: string, arg1: () => any) => void; }; }} stm
     * @param {{ start: any; end: any; }} pos
     * @param {any} className
     */
    markSubordinate(stm: any, pos: any, className: any): CodeMirror.TextMarker<CodeMirror.MarkerRange>;
    /**
     * @param {{ text: string | undefined; start: any; end: any; }} stm
     * @param {{ bp: any; ep: number; }} loc
     */
    _subregion(stm: any, loc: any): {
        start: CodeMirror.Position;
        end: CodeMirror.Position;
    };
    /**
     * Hack to apply MarkedSpan CSS class formatting and attributes to widgets
     * within mark boundaries as well.
     * (This is not handled by the native CodeMirror#markText.)
     * @param {any} start
     * @param {any} end
     * @param {{ className: string; attributes: {}; }} mark
     */
    _markWidgetsAsWell(start: any, end: any, mark: any): void;
    /**
     * Hack contd: negates effects of _markWidgetsAsWell when mark is cleared.
     * @param {any} start
     * @param {any} end
     */
    _unmarkWidgets(start: any, end: any): void;
    /**
     * Final hack: adjust class of widget when active selection is manipulated
     * by mark-selection addon.
     */
    _adjustWidgetsInSelection(): void;
    getCursor(): CodeMirror.Position;
    /**
     * @param {{ line: number; ch: number; }} c1
     * @param {{ line: number; ch: number; }} c2
     */
    cursorLess(c1: any, c2: any): boolean;
    /**
     * @param {{ end: any; }} stm
     */
    cursorToEnd(stm: any): void;
    /**
     * Checks whether the range from start to end consists solely of
     * whitespaces.
     * @param {CodeMirror.Position} start starting position ({line, ch})
     * @param {CodeMirror.Position} end ending position ({line, ch})
     */
    onlySpacesBetween(start: any, end: any): boolean;
    /**
     * @param {string} str
     */
    _onlySpaces(str: any): boolean;
    /**
     * @param {{ getDoc: () => any; }} editor
     * @param {{ from: any; }} evt
     */
    onCMChange(editor: any, evt: any): void;
    invalidateAll(): void;
    /**
     * @param {HTMLElement} dom
     */
    _markFromElement(dom: any): CodeMirror.TextMarker<CodeMirror.MarkerRange | CodeMirror.Position>;
    /**
     * Highlights the sentence mark under the mouse cursor and emits
     * onMouseEnter/onMouseLeave when the active mark changes.
     * @param {JQuery.MouseMoveEvent} evt event object
     */
    onCMMouseMove(evt: any): void;
    /**
     * De-highlights and emits onMouseLeave when leaving the active mark.
     * @param {JQuery.MouseLeaveEvent} evt event object
     */
    onCMMouseLeave(evt: any): void;
    /**
     * @param {{ key: string; }} evt
     */
    keyHandler(evt: any): void;
    /**
     * XXXX
     * @function _config
     * @memberof CmCoqProvider
     * @static
     */
    static _config(): void;
    /**
     * Iterates (non-whitespace) tokens beginning at `from`.
     * @param {*} from {line, ch} location (zero-based, CM-style)
     */
    iterTokens(from: any): Generator<{
        start: number;
        end: number;
        string: string;
        type: string;
        state: any;
        line: any;
    }, void, unknown>;
    /**
     * @param {string} text
     * @param {any} filename
     */
    load(text: any, filename: any, dirty?: boolean): void;
    shareHastebin(): void;
    shareGist(): void;
    /**
     * @param {JQuery<HTMLElement>} input
     * @param {JQuery<HTMLElement>} list
     */
    _setupTabCompletion(input: any, list: any): void;
    /**
     * @param { * } input
     * @param { * } list
     */
    _complete(input: any, list: any): void;
    static _set_keymap(): void;
}
/**
 * For HTML-formatted Coq snippets created by coqdoc.
 * This reverses the modifications made during pretty-printing
 * to allow the text to be placed in an editor.
 */
export declare class Deprettify {
    static REPLACES: [RegExp, string][];
    /**
     * Remove redundant leading and trailing newlines generated by coqdoc.
     * @param {HTMLElement} element
     */
    static trim(element: any): any;
    /**
     * @param {ChildNode} node
     */
    static isWS(node: any): any;
    /**
     * @param {ChildNode} node
     */
    static isBR(node: any): boolean;
    /**
     * Translate back some unicode symbols to their original ASCII.
     * @param {string} text
     */
    static cleanup(text: any): any;
}
export {};
//# sourceMappingURL=cm-provider.d.ts.map