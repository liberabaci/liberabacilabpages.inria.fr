/// <reference types="node" />
import { EventEmitter } from 'events';
import { Future } from '../../../backend/future';
import { CoqWorker } from '../../../backend/coq-worker';
import { FormatPrettyPrint } from '../../format-pprint/js';
import { FSInterface } from '../../cli/build/fsif';
import { CoqProject } from '../../cli/build/project';
declare class HeadlessCoqWorker extends CoqWorker {
    constructor(base_path: any);
    static instance(): any;
}
/**
 * A manager that handles Coq events without a UI.
 */
declare class HeadlessCoqManager {
    coq: CoqWorker & {
        options?: any;
    };
    volume: FSInterface;
    provider: any;
    pprint: FormatPrettyPrint;
    packages: PackageDirectory;
    project: CoqProject;
    options: any;
    doc: any[];
    when_done: Future<void>;
    startup_time: number;
    startup_timeEnd: number;
    static SDK_DIR: string;
    constructor(basePath: string, sdkDir?: string);
    start(): Promise<void>;
    getPackagePath(pkg: string): string;
    getLoadPath(): string[][][];
    goNext(): boolean;
    eof(): void;
    require(module_name: string, import_?: boolean): void;
    load(vernac_filename: string): void;
    retract(): void;
    terminate(): void;
    performInspect(inspect: any): void;
    coqCoqInfo(): void;
    coqReady(): void;
    coqLog([lvl]: [any], msg: any): void;
    coqPending(sid: any): void;
    coqAdded(sid: any): void;
    feedProcessed(sid: any): void;
    coqGot(filename: any, buf: any): void;
    coqCancelled(sid: any): void;
    coqCoqExn({ loc, pp: msg }: {
        loc: any;
        pp: any;
    }): void;
    feedFileLoaded(): void;
    feedFileDependency(): void;
    feedProcessingIn(): void;
    feedAddedAxiom(): void;
    feedMessage(sid: any, [lvl]: [any], loc: any, msg: any): void;
    findPackageDir(dirname?: string): string;
    _isDirectory(path: any): boolean;
    _identifierWithin(id: any, modpath: any): boolean;
    _format_loc(loc: any): string;
}
declare class PackageDirectory extends EventEmitter {
    dir: string;
    packages_by_name: {
        [name: string]: PackageManifest;
    };
    packages_by_uri: {
        [name: string]: PackageManifest;
    };
    _plugins: Promise<void>;
    constructor(dir: any);
    clear(): void;
    loadPackages(uris: string | string[]): Promise<void>;
    unzip(uri: string): Promise<any>;
    listModules(pkg: string | PackageManifest): string[];
    /**
     * Copy native plugin libraries (`.cmxs`; only for native mode).
     * @param binDir
     */
    appropriatePlugins(binDir: string): Promise<void>;
    ln_sf(target: string, source: string): void;
}
declare type PackageManifest = {
    name: string;
    modules: {
        [name: string]: {
            deps?: string[];
        };
    };
};
export { HeadlessCoqWorker, HeadlessCoqManager, PackageDirectory };
//# sourceMappingURL=headless.d.ts.map