import { CoqProject, SearchPath, SearchPathElement } from './project';
declare class Workspace {
    projs: {
        [name: string]: CoqProject;
    };
    searchPath: SearchPath;
    bundleName: string;
    pkgDir: string;
    outDir: string;
    open(jsonFilename: string, rootdir?: string, opts?: Workspace.Options): void;
    loadDeps(pkgs: string[], baseDir?: string): Promise<void>;
    addProject(proj: CoqProject): void;
    openProjects(pkgs: any, baseDir: string, opts?: Workspace.Options): void;
    openProjectDirect(nameOrPackage: string, baseDir: string, prefix: string, dirPaths: string[]): void;
    createBundle(filename: string): {
        manifest: {
            name: string;
            deps: any[];
            pkgs: any[];
            chunks: any[];
        };
        filename: string;
        save(): void;
    };
    listPackageContents(nameFilter?: string | RegExp): Workspace.PackagesAndContents;
    _filt(e: undefined | string | RegExp): (s: string) => boolean;
}
declare namespace Workspace {
    type Options = {
        ignoreMissing?: boolean;
    };
    type PackagesAndContents = {
        [pkg: string]: SearchPathElement[];
    };
}
export { Workspace };
//# sourceMappingURL=workspace.d.ts.map