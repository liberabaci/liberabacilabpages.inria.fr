/// <reference types="node" />
/// <reference types="node" />
import fs from 'fs';
import path from 'path';
declare type FSInterface = {
    fs: typeof fs;
    path: typeof path;
};
declare const fsif_native: FSInterface;
export { FSInterface, fsif_native };
//# sourceMappingURL=fsif.d.ts.map