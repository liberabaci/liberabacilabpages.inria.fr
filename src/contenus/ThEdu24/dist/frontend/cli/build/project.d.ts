/// <reference types="node" />
/// <reference types="node" />
import path from 'path';
import { FSInterface } from './fsif';
import { CoqDep } from './coqdep';
import type JSZip from 'jszip';
import { ZipOptions } from 'fflate';
declare const fs: typeof import("fs");
declare class CoqProject {
    volume: FSInterface;
    name: string;
    deps: string[];
    searchPath: SearchPath;
    opts: PackageOptions;
    _modules: SearchPathElement[];
    constructor(name?: string, volume?: FSInterface);
    fromJson(json: {
        [root: string]: {
            prefix?: string;
            dirpaths?: DirPathFlag[];
        };
    }, baseDir?: string, volume?: FSInterface): this;
    fromDirectory(dir?: string, volume?: FSInterface): this;
    /**
     * Configures a project from flags in _CoqProject format, i.e. -R ..., -Q ...,
     * and list of .v files.
     * @param {string} coqProjectText content of _CoqProject definition file
     * @param {string} baseDir project root directory (usually the one containing _CoqProject)
     * @param {FSInterface} volume containing volume
     */
    fromCoqMF(coqProjectText: string, baseDir?: string, volume?: FSInterface): this;
    setModules(mods: (string | SearchPathElement)[]): void;
    computeDeps(): CoqDep;
    modules(): SearchPathElement[] | Generator<SearchPathElement, void, unknown>;
    modulesByExt(ext: string): Generator<SearchPathElement, void, unknown>;
    modulesByExts(exts: string[]): Generator<SearchPathElement, void, unknown>;
    listModules(exts?: string | string[]): Set<string>;
    createManifest(): {
        name: string;
        deps: string[];
        modules: any;
    };
    toDirPath(flag: DirPathFlag): string[];
    /**
     *  Read project file and store all .v files in transient folders
     * according to their logical paths.
     */
    copyLogical(store?: InMemoryVolume): CoqProject;
    toZip(withManifest?: string, extensions?: string[], prep?: PackagePreprocess): Promise<Zipped>;
    toPackage(filename?: string, extensions?: string[], prep?: PackagePreprocess, postp?: PackagePostprocess): Promise<PackageResult>;
}
declare type DirPathFlag = string | {
    logical: string;
    rec?: boolean;
};
declare type PackageOptions = {
    json: any;
    zip: ZipOptions;
};
declare class Zipped {
    z: {
        [filename: string]: Uint8Array;
    };
    opts?: ZipOptions;
    constructor(opts?: ZipOptions);
    writeFileSync(filename: string, content: string | Uint8Array): void;
    zipSync(): Uint8Array;
}
declare class PackageResult {
    pkg: {
        filename: string;
        zip: Zipped;
    };
    manifest: {
        filename: string;
        json: string;
        object: any;
    };
    constructor(pkg: {
        filename: string;
        zip: Zipped;
    }, manifest: {
        filename: string;
        json: string;
        object: any;
    });
    save(bundle?: {
        chunks?: any[];
    }): Promise<PackageResult>;
}
declare type PackagePreprocess = (mod: SearchPathElement) => (SearchPathElement & {
    ext?: string;
    payload?: Uint8Array;
})[];
declare type PackagePostprocess = (manifest: any, archive?: string) => any;
declare class SearchPath {
    volume: FSInterface;
    path: SearchPathElement[];
    moduleIndex: ModuleIndex;
    packageIndex: PackageIndex;
    constructor(volume?: FSInterface);
    add({ volume, physical, logical, pkg }: SearchPathAddParameters): void;
    addRecursive({ volume, physical, logical, pkg }: SearchPathAddParameters): void;
    addFrom(other: SearchPath | CoqProject): void;
    has({ volume, physical, logical, pkg }: SearchPathAddParameters): SearchPathElement;
    toLogical(filename: string): string[];
    toDirPath(name: string | string[]): string[];
    modules(): Generator<SearchPathElement>;
    modulesOf(pkg?: string): Generator<SearchPathElement, void, unknown>;
    modulesByExt(ext: string): Generator<SearchPathElement, void, unknown>;
    modulesByExts(exts: string[]): Generator<SearchPathElement, void, unknown>;
    listModules(): Set<string>;
    listModulesOf(pkg?: string): Set<string>;
    _listNames(modules: Iterable<SearchPathElement>): Set<string>;
    findModules(prefix: string | string[], name: string | string[], exact?: boolean): Generator<SearchPathElement | {
        volume: any;
        physical: any;
        logical: string[];
        pkg: string;
    }, void, unknown>;
    _findModules(prefix: string | string[], name: string | string[], exact?: boolean): Generator<SearchPathElement, void, unknown>;
    _findExtern(prefix: string | string[], name: string | string[], exact?: boolean): Generator<{
        volume: any;
        physical: any;
        logical: string[];
        pkg: string;
    }, void, unknown>;
    createIndex(): ModuleIndex;
}
declare type SearchPathAddParameters = {
    volume?: FSInterface;
    logical: string[] | string;
    physical: string;
    pkg?: string;
};
declare type SearchPathElement = {
    volume: FSInterface;
    logical: string[];
    physical: string;
    pkg?: string;
};
/**
 * @todo there is some duplication with backend's PackageIndex.
 */
declare class ModuleIndex {
    index: Map<string, SearchPathElement>;
    constructor();
    add(mod: SearchPathElement): void;
    findModules(prefix: string | string[], suffix: string | string[], exact?: boolean): Generator<SearchPathElement, void, unknown>;
}
interface PackageIndex {
    findModules(prefix: string | string[], suffix: string | string[], exact?: boolean): Iterable<string>;
    findPackageDeps(prefix: string | string[], suffix: string | string[], exact?: boolean): Set<string>;
}
declare abstract class StoreVolume implements FSInterface {
    fs: typeof fs;
    path: typeof path;
    abstract _files: Iterable<string>;
    constructor();
    readdirSync(dir: string): any[];
    statSync(fp: string): {
        isDirectory: () => boolean;
    };
}
declare class InMemoryVolume extends StoreVolume {
    fileMap: Map<string, Uint8Array>;
    constructor();
    get _files(): IterableIterator<string>;
    writeFileSync(filename: string, content: Uint8Array | string): void;
    readFileSync(filename: string, encoding?: string): string | Uint8Array;
    statSync(fp: string): {
        isDirectory: () => boolean;
    };
    renameSync(oldFilename: string, newFilename: string): void;
    unlinkSync(filename: string): void;
}
declare class ZipVolume extends StoreVolume {
    zip: JSZip; /** @todo use fflate */
    _files: string[];
    constructor(zip: JSZip);
    readFileSync(filename: string, encoding?: string): any;
    statSync(fp: string): {
        isDirectory: () => boolean;
    };
    static fromFile(zipFilename: string): Promise<ZipVolume>;
    static fromBlob(blob: Blob | Promise<Blob>): Promise<ZipVolume>;
    _inflateSync(entry: any): any;
}
declare class JsCoqCompat {
    /**
     * Runs js_of_ocaml to convert .cma bytecode files to .cma.js.
     * @param mod
     */
    static transpilePluginsJs(mod: SearchPathElement): (SearchPathElement | {
        physical: string;
        ext: string;
        volume: FSInterface;
        logical: string[];
        pkg?: string;
    })[];
    static flags(): "" | "--pretty --noinline --disable shortvar --debug-info";
    /**
     * Converts a package manifest to (older) jsCoq format.
     * @param manifest original JSON manifest
     * @param pkgfile `.coq-pkg` archive filename
     * @obsolete Package manifest format is now consistent between js/wa.
     */
    static backportManifest(manifest: any, pkgfile: string): {
        name: any;
        deps: any;
        archive: any;
        pkgs: {
            pkg_id: string[];
            vo_files: string[][];
            cma_files: string[][];
        }[];
        modDeps: {};
    };
}
export { CoqProject, SearchPath, SearchPathElement, PackageOptions, ModuleIndex, InMemoryVolume, ZipVolume, JsCoqCompat };
//# sourceMappingURL=project.d.ts.map