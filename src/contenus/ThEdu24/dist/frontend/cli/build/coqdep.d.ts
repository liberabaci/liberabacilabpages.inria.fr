import { FSInterface } from './fsif';
import { SearchPath, SearchPathElement } from './project';
declare class CoqDep {
    volume: FSInterface;
    searchPath: SearchPath;
    deps: {
        from: SearchPathElement;
        to: SearchPathElement[];
    }[];
    extern: Set<string>;
    constructor(volume?: FSInterface);
    processPackage(pkg: string): void;
    processModules(modules: Iterable<SearchPathElement>): void;
    processModule(mod: SearchPathElement): void;
    processVernacFile(filename: string, mod?: SearchPathElement): void;
    processVernac(v_text: string, mod: SearchPathElement): void;
    /**
     * Gets the list of modules that `mod` depends on.
     * @note looks for the module by physical filename.
     */
    depsOf(mod: SearchPathElement): SearchPathElement[];
    depsToJson(): {};
    /**
     * Basically, topological sort.
     * @todo allow parallel builds?
     */
    buildOrder(modules?: SearchPathElement[] | Generator<SearchPathElement>): SearchPathElement[];
    _topologicalSort(vertices: string[], adj: Map<string, string[]>): any[];
    _extractImports(v_text: string): Generator<SearchPathElement | {
        volume: any;
        physical: any;
        logical: string[];
        pkg: string;
    }, void, unknown>;
    _extractImportsBase(v_text: string): Generator<SearchPathElement | {
        volume: any;
        physical: any;
        logical: string[];
        pkg: string;
    }, void, unknown>;
    _resolve(prefix: string, suffix: string): Generator<SearchPathElement | {
        volume: any;
        physical: any;
        logical: string[];
        pkg: string;
    }, void, unknown>;
    _getExtern(deps: SearchPathElement[]): Generator<string, void, undefined>;
    _uniqBy<T>(gen: Generator<T>, key: (v: T) => string): Generator<T, void, unknown>;
}
export { CoqDep };
//# sourceMappingURL=coqdep.d.ts.map