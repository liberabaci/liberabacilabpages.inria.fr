declare function cat(fn: string): string;
/**
 * If `fn` contains `expectedValue`, do nothing;
 * Otherwise run `whenNeq` and update `fn`.
 * @returns `true` iff `fn` already contained `expectedValue`.
 */
declare function cas(fn: string, expectedValue: string, whenNeq: () => void | Promise<void>): Promise<boolean>;
declare function dirstamp(fn: string): string;
declare function ln_sf(target: string, source: string): void;
declare function existsExec(p: any): number | false;
declare function existsDir(p: any): boolean;
export { cat, cas, dirstamp, ln_sf, existsExec, existsDir };
//# sourceMappingURL=shutil.d.ts.map