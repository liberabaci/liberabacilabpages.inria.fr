import type commander from 'commander';
declare function setup(basedir?: string, includeNative?: boolean): Promise<{
    coqlib: string;
    include: string;
}>;
declare function main(args: string[]): Promise<any>;
declare function installCommand(commander: commander.CommanderStatic): void;
export { main, setup, installCommand };
//# sourceMappingURL=setup.d.ts.map