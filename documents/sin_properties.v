Require Import Reals Lra.
From Coquelicot Require Import Coquelicot.
Require Import Interval.Tactic Interval.Plot.
Open Scope R_scope.

Require Import Classical.

Ltac either_or_not P := case (classic P).

Ltac check := lra.

(* This property can be visualized using gnuplot by typing
  echo "set xrange [0:10]; plot x, sin(x)" | gnuplot -p
*)
Lemma sinx_ltx x : 0 < x -> sin x < x.
Proof.
intros xgt0.
enough (0 < x - sin x) by check.
(* Cut the proof in two parts: after PI/2 and before. *)
assert (right_part : PI / 2 < x -> 0 < x - sin x).
  assert (sin x <= 1) by (assert (tmp := SIN_bound x); check).
  assert (1 < PI / 2) by exact PI2_1.
  check.
assert (left_part : ~ (PI / 2 < x) -> 0 < x - sin x).
  intros xsmall.
(* Show the value of the derivative. *)
  assert (der : forall c, 0 <= c <= x -> 
         derivable_pt_lim (fun x => x - sin x) c
           (1 - cos c)).
    intros c _.
    auto_derive; check.
  assert (exists c, x - sin x - (0 - sin 0) =
              (1 - cos c) * (x - 0) /\ 0 < c < x)
    as [c [feq cint]].
    apply (MVT_cor2 (fun x => x - sin x) (fun x => 1 - cos x) _ _ xgt0 der).
  rewrite sin_0, !Rminus_0_r in feq. 
  rewrite feq.
  apply Rmult_lt_0_compat; cycle 1.
    check.
  enough (cos c < 1) by check.
  rewrite <- cos_0.
  apply cos_decreasing_1.
          check.
        check.
      check.
    check.
  check.
either_or_not (PI / 2 < x); check.
Qed.
