(* Coercions *)
Axiom (A B : Type) (a : A) (f : A -> B).
Coercion f : A >-> B.
Check (a : B).

(* Typeclasses *)
Class hasZ T := mkHasZ {z' : T}.
Instance nat_hasZ : hasZ nat := mkHasZ _ 0.
Check (z' : nat).

(* Structures *)
Structure Z := mkZ {zType :> Type; z : zType}.
Arguments z {z}.
Canonical natZ := mkZ nat 0.

Check (z : nat).

Check (nat : Z). (* NEW! *)
Check (@z (nat : Z)).




