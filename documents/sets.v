From Coq Require Program.Tactics.
From Coq Require Import Lia.
From Coq Require Import PeanoNat.
From Coq Require Import ssreflect ssrbool ssrfun.
Unset Universe Checking.

(* Reasonable set of axioms for doing math *)
Axiom propext : forall (P Q : Prop), (P <-> Q) -> P = Q.
Axiom PI : forall (P : Prop) (p q : P), q = p.
Axiom dfunext : forall A (B : A -> Type) 
  (f g : forall x, B x), (forall x, f x = g x) -> f = g.
Arguments PI {P}.

(* Add typeclass resolution to trivial things
   (maybe dangerous in general, must be restricted) *)
Ltac done :=
  trivial; hnf; intros; solve
   [ do ![solve [trivial | apply: sym_equal; trivial]
         | discriminate | contradiction | split]
   | case not_locked_false_eq_true; assumption
   | match goal with H : ~ _ |- _ => solve [case H; trivial] end 
   | typeclasses eauto ].

(* Workaround the absence of coercion default class *)
Definition target := @id Type.
Arguments target /.
#[global] Hint Unfold target : typeclass_instances.
#[global] Typeclasses Transparent target.
Notation "[ x ]" := (ltac:(
  (let T' := type of x in let T := eval simpl in T' in
  match goal with 
  | |- target ?X => refine (x : X)
  | |- ?X => refine (x : target T')
  | |- ?X => refine ((x : T') : target X)
  | |- ?X' => let X := eval simpl in X' in refine ((x : target T') : X)
  | |- ?X' => let X := eval simpl in X' in refine (((x : T') : target X) : X)
  end) || refine x)) (only parsing).
Notation "[ x :> T ]" := ([x] : T) (only parsing).
Notation "[ x ]" := (x : target _) (only printing).
Notation "[ x ]" := ((x : _) : _) (only printing).
Notation "[ x ]" := (((x : _) : _) : _) (only printing).
Notation "[ x ]" := ((((x : _) : _) : _) : _) (only printing).
Definition show (T : Type) (x : T) := x.
Notation "( x : T )" := (@show T x) (only printing).
Definition show_type (T : Type) (x : T) : (x : T) = @show T x := erefl.

(* Design pattern for sets as subtypes *)
Structure set (T : Type) := MkSet {
  set_to_pred : T -> Prop
}.
Arguments set_to_pred : simpl never.
Notation "[ 'set' x | P ]" := (MkSet _ (fun x => P))
  (x name, format "[ 'set'  x  |  P ]").
Notation "[ 'set!' P ]" := (MkSet _ P)
  (P at level 200, format "[ 'set!'  P ]").

Canonical generic_set T (P : T -> Prop) := MkSet T P.
Notation "[ P ]" := (generic_set _ P) (only printing).

Class mem T (X : set T) (x : T) : Prop := Mem { IsMem : set_to_pred _ X x }.
Notation "x \in X" := (@mem _ X x).
#[global] Hint Mode mem - - ! : typeclass_instances.

Structure memType T (X : set T) := MemType {
  this :> target T;
  memP : this \in X
}.
Arguments this {T X}.
Arguments memP {T X}.
Hint Extern 0 (_ \in _) => solve [apply: memP] : core.
Hint Extern 0 (this _ \in _) => solve [apply: memP] : typeclass_instances.

Definition set_to_pred_overlay (T : Type) (X : set T) := fun x => x \in X. 
Arguments set_to_pred_overlay /.
Coercion set_to_pred_overlay : set >-> Funclass.

Lemma setE T (P : T -> Prop) x : x \in [set! P] = P x.
Proof. by apply/propext; split=> // -[]. Qed.

Lemma eq_set T (X Y : set T) : X = Y <-> (forall x, (x \in X) = (x \in Y)).
Proof.
split=> [->//|]; case: X Y => [P] [Q] /= eqXY.
by congr MkSet; apply/dfunext => x; have := eqXY x; rewrite !setE.
Qed.

Lemma eq_setP T (X Y : set T) : X = Y <-> (forall x, (x \in X) <-> (x \in Y)).
Proof. by rewrite eq_set; split=> XY x; [rewrite XY//|apply/propext]. Qed.

Canonical generic_memType T (X : set T) t tP := MemType T X t tP. 
Notation "[ t ]" := (@generic_memType _ _ t _) (only printing).

Coercion mem_of_set T (X : set T) : Type := memType T X.

Lemma setP_def {T} {P : T -> Prop} (x : [set! P]) : P [x :> T].
Proof. by case: x => // ? []. Qed.

Notation setP X x := (setP_def ((x : target _) : X)).
Notation "X .P x" := (setP X x) (format "X .P  x", at level 0).

Class hasAdd T := HasAdd { add_op : T -> T -> T }.
#[global] Hint Mode hasAdd ! : typeclass_instances.

Structure addType := AddType {
  add_sort :> Type;
  add_of_addType : hasAdd add_sort;
}.
Coercion add_sort_target (T : addType) : target Type := add_sort T.
Definition add {T} := (@add_op _ (add_of_addType T)).
Notation "x +_ T y" := (@add T [x :> T] [y :> T] : T)
   (T at level 0, at level 60, format "x  +_ T  y", only parsing).
(* Notation "x + y" := (@add _ x [y]) (only parsing). *)
Notation "a + b" := (@add _ a b).

(* Workaround bug introduced by the parsing notation for + in patterns *)
(* Declare Scope pat_scope. *)
(* Delimit Scope pat_scope with P. *)
(* Notation "x + y" := (@add _ x y) (only parsing) : pat_scope. *)

Canonical generic_addType T a := AddType T a.
Notation "[ T ]" := (generic_addType T _) (only printing).

(* why didn't Typeclasses Transparent work? *)
#[global] Hint Extern 1 (hasAdd (target _)) =>
  rewrite /target : typeclass_instances.

#[reversible]
Coercion target_to_add_sort (T : addType) (x : T) : target T := x.

Class isSubAdd (T : addType) (dom : set T) := IsSubAdd {
  isSubAddP : forall a b, dom a -> dom b -> dom (a + b)
}.
#[global] Hint Mode isSubAdd - ! : typeclass_instances.

Structure addSet (T : addType) := SubAddType {
  set_to_subadd :> set T;
  subAddP_subdef : isSubAdd _ set_to_subadd;
}.
Canonical generic_addSet T P PP := SubAddType T P PP.
Notation "[ P ]" := (@generic_addSet _ P _) (only printing).

Lemma mem_add (T : addType) (P : addSet T)
   (x : P) (y : P) : x +_T y \in P.
Proof. by apply: isSubAddP => //; apply: subAddP_subdef. Qed.
#[global] Hint Extern 1 (@add ?T _ _ \in _) => 
  apply: (@mem_add T) : typeclass_instances.

Program Definition generic_sub_add (T : addType) (P : addSet T) : hasAdd P :=
   HasAdd P (fun (x y : P) => MemType _ _ ([x :> T] + [y :> T]) _).
#[global] Hint Extern 1 (hasAdd _) =>
  apply: generic_sub_add : typeclass_instances.

Lemma add_sub (T : addType) (P : addSet T) (x y : P) :
  x +_P y = x +_T y :> T.
Proof. by []. Qed.

Definition set_add (T : addType) : hasAdd (set T) :=
   HasAdd (set T) (fun X Y => [set z | exists (x : X) (y : Y), z = x +_T y]).
#[global] Hint Extern 1 (hasAdd (set _)) => 
  eapply set_add : typeclass_instances.

(* This is a mixin and shouldn't be a class in the compiled design pattern *)
Class add_is_AC (T : addType) := AddIsAc
 { addA_subproof : associative (@add T);
   addC_subproof : commutative (@add T) }.
#[global] Hint Mode add_is_AC ! : typeclass_instances.

Class has_AcAdd (T : Type) := HasAcAdd
 {
  has_add_of_addac : hasAdd T;
  ac_of_addac :> add_is_AC T
 }.
#[global] Hint Mode has_AcAdd ! : typeclass_instances.

Structure acAddType := AcAddType {
  addac_sort :> Type;
  addac_of_acaddType : has_AcAdd addac_sort;
}.
Coercion addac_sort_target (T : acAddType) : target Type := addac_sort T.
#[global] Instance add_addac (T : acAddType) : hasAdd T := 
  @has_add_of_addac _ (addac_of_acaddType T).
Coercion addac_to_add (T : acAddType) : addType :=
  generic_addType T (add_addac T).

Canonical generic_acAddType (T : Type) a := AcAddType T a.
Notation "[ T ]" := (@generic_acAddType T _) (only printing).

(* #[global] Instance target_add_is_AC (T : acAddType) : has_AcAdd (target T). *)
(* Proof. *)
(* by eexists; rewrite /target/=; apply: ac_of_addac; apply: addac_of_acaddType. *)
(* Defined. *)

Lemma addA (T : acAddType) : associative (@add T).
Proof. by apply: addA_subproof; apply: ac_of_addac. Qed.

Lemma addC (T : acAddType) : commutative (@add T).
Proof. by apply: addC_subproof; apply: ac_of_addac. Qed.

Lemma addAC (T : acAddType) : interchange (@add T) (@add T).
Proof.
move=> x y z t; rewrite !addA.
by congr (_ + _); rewrite -!addA; congr (_ + _); apply: addC.
Qed.

Lemma subadd_add (T : acAddType) (P Q : addSet T) : isSubAdd T (P +_(set T) Q).
Proof.
split=> _ _ [[a1 [a2 ->]]] [[b1 [b2 ->]]].
by rewrite addAC; exists; exists (a1 + b1), (a2 + b2).
Qed.
#[global] Hint Extern 1 (@isSubAdd ?T (@add _ _ _)) =>
  apply: (@subadd_add (@AcAddType T _)) : typeclass_instances.

(******************************)
(* The real demo begins here. *)
(******************************)

(* I. Type structures *)
(* We declare nat is an addType *)
(* The next 4 lines should be DSL directives *)
#[global] Instance nat_add : hasAdd nat := HasAdd nat plus.
#[global] Instance nat_add_is_AC : add_is_AC nat.
Proof. split; [exact: Nat.add_assoc | exact: Nat.add_comm]. Qed.
#[global] Program Instance nat_add_AC : has_AcAdd nat.

(* Thanks to these declarations and the setup *)
(* All of these are addTypes *)
(* The sets of a type with addition can be provided with an addition as well *)
(* (see Hint set_add above) *)
(* This is an an example of stability at typelevel *)
(* Inference is automatic *)
Check nat                 : addType.
Check set nat             : addType.
Check set (set nat)       : addType.
Check set (set (set nat)) : addType.

(* II. Set structure *)
(* Even is a set, but it can be used as a type, see  *)
Definition even : set nat := [set n | exists m, n = 2 * m].
#[global] Typeclasses Opaque even.

(* We declare that even is additively stable and contains 0 *)
(* The next 12 lines should be DSL directives *)
#[global]
Program Definition even_subAdd : isSubAdd nat even := IsSubAdd _ _ _.
Next Obligation.
have [m/= ->] := even.P a. have [n/= ->] := even.P b.
by exists; exists (m + n); rewrite /add/=; lia.
Qed.
#[global] Hint Extern 0 (isSubAdd _ _) =>
  apply: even_subAdd : typeclass_instances.

Lemma even0 : 0 \in even. Proof. by exists; exists 0. Qed.
#[global] Hint Extern 0 (_ \in even) =>
  apply: even0 : typeclass_instances.

(*
  This is example of set stability property:
  additively stable subsets of a type with associative-commutative addition
  are preserved by set level addition.
  (see Hint subadd_add above)
*)

Goal isSubAdd nat (even + even). Proof. by []. Qed.

Check even +_(set nat) even : addSet nat.
Check even +_(set nat) even +_(set nat) even : addSet nat.
Check even +_(set nat) even +_(set nat) even +_(set nat) even : addSet nat.
Check [even + even :> set nat] : addSet nat.

(* III. Elements *)
(* Their stability is deduced from the structures of the sets *)
(* Here stability by + and 0 from the structure of the sets is *)
(* reused for automated inference of membership *)
(*
Note there is barely a difference between
 - (n : nat) (_ : n \is even)
 - and (n : even)
In some cases an explicit generic "[]" is needed.
More work needed (modifying Coq?) to remove the generic [] coercion?
*)
Section tests.
Variables (Peven : even -> Prop) (Pnateven : forall n, n \in even -> Prop).
Variables (n : nat) (n_even : n \in even) (m : even).

Check Peven m : Prop. (* trivial *)
Check Pnateven [m] _ : Prop. (* coercion from even to nat, proof auto *)
Check Peven [n] : Prop. (* reconstruction of even from nat *)
Check Pnateven n _ : Prop. (* proof found automatically *)

(* all the following are possible and the first two equivalent *)
Check n + m : nat.
Check n +_nat m.
Check m +_nat n.

(* all the following are possible and the first two equivalent *)
Check m + [n] : even.
Check m +_even n.
Check n +_even m.
Check [n + m] : even.

(* Since the inference is based on the first argument, 
   we might need a little [] fix to do the other direction *)
Check [n :> even] + m.
Check [m :> nat] + n.
Check [m :> nat] + n : nat.

End tests.

Definition add_even (m n : even) : m +_nat n \in even.
(* automatic inference of even by stability by addition *)
Proof. by []. Qed.

Lemma even2P (m n : even) : m +_nat n \in even + even.
(* automatic resolution from context *) 
Proof. by split; exists m, n. Qed.

(* Bonus inherited from level 2: *)
(* even + even automatically preserves addition of natural numbers too *)
Lemma add_even2 (m n : even +_(set nat) even) : m +_nat n \in even + even.
Proof. by []. Qed.

(* Automatic inference makes proofs really easy *)
Lemma evenDeven_eq_even : even + even = even.
Proof.
apply/eq_setP => n; split=> [|n_even].
  (* automatic inference of even by stability by addition, again *)
  by move=> [[? [? ->]]].
(* automated reverse coercions using context *)
by split; exists [0], [n].
(* Here, the design pattern fell a little bit short
   forcing explicit, albeit generic, [x] reverse coercions *)
Qed.


